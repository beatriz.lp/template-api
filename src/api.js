const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid } = require("uuid")

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();
  const db = mongoClient.db().collection("users");

  function isEmailValid(email){
	const regex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
	return regex.test(email) && !db.findOne(email);
}

const regexPassword = /([A-zÁ-ú]|[0-9]){8,32}/

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (req, res) => res.json("Hello, World!"));

  api.post("/users", (req, res) => {
    const user = {
      id: uuid(),
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
      passwordConfirmation: req.body.passwordConfirmation
    }

      for (const fieldName of ['name', 'email', 'password', 'passwordConfirmation']) {
        if (!user[fieldName]) {
          return res.status(400).json({
            error: `Request body had missing field ${fieldName}`
          });
        }
      } if(!isEmailValid(user.email)){
        return res.status(400).json({
          error: `Request body had malformed field email`
        });
      } else if (!regexPassword.test(user.password)){
        return res.status(400).json({
          error: `Request body had malformed field password`
        });
    } else if(user.password !== user.passwordConfirmation){ 
        return res.status(422).json({
          error: "Password confirmation did not match"
      })}

      const userCreatedMessage = {
        eventType: 'UserCreated',
        entityId: user.id,
        entityAggregate: { 
          name: user.name,
          email: user.email,
          password: user.password,
        },
      };
      stanConn.publish('users', JSON.stringify(userCreatedMessage));
  
      return res.status(201).json({
        user: {
          id: user.id,
          name: user.name,
          email: user.email
      }})
    
  });

  api.delete("/users/:uuid", async (req, res) => {
    const authorizationHeader = req.get("Authentication");
    const { uuid } = req.params
    const match = /Bearer (.+)/.exec(authorizationHeader);
    const token = match[1];
    const decoded = jwt.decode(token);

    try {
      if (!authorizationHeader) {
        return res.status(401).json({
          error: "Access Token not found"
        });
      } else if (decoded.id !== uuid) {
        return res.status(403).json({
          error: "Access Token did not match User ID"
        });
      }
      const userDeletedMessage = {
        eventType: 'UserDeleted',
        entityId: uuid,
        entityAggregate: {},
      };
      stanConn.publish('users', JSON.stringify(userDeletedMessage));  
      
      const user = await db.findOne({ id: uuid })
      if (!user) {
      res.status(400).json({error: "User doesn't exist"})
      }

      res.status(200).json({
        id: uuid
      });
  
  } catch (err) {
    console.error(err)
    return res.json(err)
  }
})

  return api;
};
