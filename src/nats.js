const stan = require("node-nats-streaming");

module.exports = function (mongoClient) {
  const conn = stan.connect("test-cluster", "test", {
    url: process.env.BROKER_URL,
  });

  conn.on("connect", () => {
    const usersCollection = mongoClient.db().collection("users");

   const userCreated = conn.subscribe("UserCreated")
   userCreated.on("message", async (msg) => {
    const data = JSON.parse(msg.getData());
    const userId = data.entityId
    const userInfo = data.entityAggregate
    try{
    await usersCollection.insertOne({ id: userId, ...userInfo});
    console.log('SUCESSO NA CRIAÇÃO')
    } catch(err){
      console.error(err)
      return res.json(err)
    }
  })

  const userDeleted = conn.subscribe("UserDeleted");
  userDeleted.on('message', async (message) => {
    const event = JSON.parse(message.getData());
    const uuid = event.entityId;

    await db.deleteOne({ id: uuid });
    console.log(`Deleted user succesfully ${uuid}`);
  })


  });

  return conn;
};
